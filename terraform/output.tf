output "storage_name" {
  value = module.StorageAccount.stg_act_name 
}

output "resource_group_name" {
  value = module.ResourceGroup.rg_name 
}

output "virtual__network_name" {
  value = module.VirtualNetwork.virtual_net_name 
}

output "SubnetName" {
  value = module.Subnet.subnet_name 
}

output "network__security_name" {
  value = module.NetworkSecurityGroup.newtork_security_group_name 
}

output "tls_private_key" {
  value     = tls_private_key.romeisa_key.private_key_pem
  sensitive = true
}
