terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.88.1"
    }
  }
}

data "docker_image" "myImage" {
  name                = "hashicorp/terraform:latest"
  resource_group_name = "romeisa"
}

provider "azurerm" {
  features {

  }
  subscription_id = "c0177cf4-3641-4a06-bbf6-40d1ac0a60f8"
  client_id       = "2d94e380-2131-491a-b2b7-8ceac152c9ed"
  client_secret   = "Y0S8Q~0s7Z3X0~gnImZ8kNMaw5i09oqz5YfXJc~2"
  tenant_id       = "bb364f09-07cf-4eca-9b92-1f26a92d5f3f"
}

module "ResourceGroup" {
  vmname = "romeisa"
  location  = "North Europe"
}

module "StorageAccount" {
  name           = "romeisa${random_id.randomId.hex}"
  resource_group_name = module.ResourceGroup.rg_name 
  location            = "North Europe"
}

resource "random_id" "randomId" {
  keepers = {
    resource_group = module.ResourceGroup.rg_name 
  }

  byte_length = 8
}

module "VirtualNetwork" {
  name           = "romeisa-vnet"
  resource_group_name = module.ResourceGroup.rg_name 
  location            = "North Europe"
}

module "Subnet" {
  name            = "default"
  resource_group_name  = module.ResourceGroup.rg_name 
  virtual_network_name = module.VirtualNetwork.virtual_net_name 
  address_prefix       = ["10.0.0.0/24"]
}

module "network_security_group" {
  name           = "romeisa-nsg"
  location            = "North Europe"
  resource_group_name = module.ResourceGroup.rg_name 
}

resource "azurerm_subnet_network_security_group_association" "romeisaSRule" {
  subnet_id                 = module.Subnet.subnet_id 
  network_security_group_id = module.NetworkSecurityGroup.newtork_security_group_id 
}

module "NetworkInterface" {
  name           = "romeisa26"
  location            = "North Europe"
  subnet_id           = module.Subnet.subnet_id 
  ip                  = azurerm_public_ip.PublicIPromeisa.id
  resource_group_name = module.ResourceGroup.rg_name 
}

resource "tls_private_key" "romeisa_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

module "VirtualMachine" {
  vmname             = "romeisaVM"
  location              = "North Europe"
  imageID               = data.azurerm_image.myImage.id
  resource_group_name   = module.ResourceGroup.rg_name 
   network_interface_id = module.NetworkInterface.Nic_ID 
}

resource "azurerm_public_ip" "PublicIPromeisa" {
  name                = "romeisa-ip"
  location            = "North Europe"
  resource_group_name = module.ResourceGroup.rg_name 
  allocation_method   = "Dynamic"
  SKU                 = "Basic"
  tier                = "regional"
} 

resource "azurerm_subnet" "romeisa_subnet" {
  name                 = var.name
  resource_group_name  = var.resource_group_name
  virtual_network_name = var.virtual_network_name
  address_prefixes     = var.address_prefix
}

resource "azurerm_resource_group" "romeisa_res" {
  name     = var.vmname
  location = var.location
}

resource "azurerm_storage_account" "romeisa" {
  name                     = "linuxvma2fd22902b"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
}
