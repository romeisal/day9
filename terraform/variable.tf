
variable "name" {
    type = string
    description = "The network interface name"
}

variable "location" {
    type = string
    description = "The location for deployment"
}

variable "resource_group_name" {
    type = string
    description = "The resource group name"
}
